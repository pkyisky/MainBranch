# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-14 13:52
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('clientRelationships', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('slot', models.CharField(choices=[('1 AM', '1 AM'), ('2 AM', '2 AM'), ('3 AM', '3 AM'), ('4 AM', '4 AM'), ('5 AM', '5 AM'), ('6 AM', '6 AM'), ('7 AM', '7 AM'), ('8 AM', '8 AM'), ('9 AM', '9 AM'), ('10 AM', '10 AM'), ('11 AM', '11 AM'), ('12 AM', '12 AM'), ('13 PM', '13 PM'), ('14 PM', '14 PM'), ('15 PM', '15 PM'), ('16 PM', '16 PM'), ('17 PM', '17 PM'), ('18 PM', '18 PM'), ('19 PM', '19 PM'), ('20 PM', '20 PM'), ('21 PM', '21 PM'), ('22 PM', '22 PM'), ('23 PM', '23 PM'), ('24 PM', '24 PM')], max_length=11)),
                ('email', models.TextField(max_length=500, null=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='schedule', to=settings.AUTH_USER_MODEL)),
                ('users', models.ManyToManyField(blank=True, related_name='scheduleUsers', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
