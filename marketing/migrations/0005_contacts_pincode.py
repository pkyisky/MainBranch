# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-06 12:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('marketing', '0004_auto_20180606_0402'),
    ]

    operations = [
        migrations.AddField(
            model_name='contacts',
            name='pinCode',
            field=models.CharField(max_length=10, null=True),
        ),
    ]
