# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-05 07:58
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recruitment', '0021_auto_20180705_0758'),
    ]

    operations = [
        migrations.AlterField(
            model_name='interview',
            name='interviewer',
            field=models.ManyToManyField(blank=True, related_name='interviwer', to=settings.AUTH_USER_MODEL),
        ),
    ]
