# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-05 07:58
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('recruitment', '0020_interview_candidate'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='interview',
            name='candidate',
        ),
        migrations.AddField(
            model_name='interview',
            name='candidate',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='candidates', to='recruitment.JobApplication'),
        ),
        migrations.AlterField(
            model_name='interview',
            name='interviewer',
            field=models.ManyToManyField(blank=True, null=True, related_name='interviwer', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='interview',
            name='mode',
            field=models.CharField(choices=[('online', 'online'), ('telephonic', 'telephonic'), ('face-to-face', 'face-to-face')], default='online', max_length=15, null=True),
        ),
        migrations.AlterField(
            model_name='interview',
            name='score',
            field=models.PositiveSmallIntegerField(default=0, null=True),
        ),
        migrations.AlterField(
            model_name='interview',
            name='status',
            field=models.CharField(choices=[('created', 'created'), ('suitable', 'suitable'), ('un-suitable', 'un-suitable'), ('recommand-other-job', 'recommand-other-job')], default='created', max_length=15, null=True),
        ),
    ]
