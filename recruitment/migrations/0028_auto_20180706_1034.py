# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-06 10:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recruitment', '0027_auto_20180706_1030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jobapplication',
            name='status',
            field=models.CharField(choices=[('Created', 'Created'), ('Screening', 'Screening'), ('SelfAssesmenent', 'SelfAssesmenent'), ('TechicalInterview', 'TechicalInterviewing'), ('HRInterview', 'HRInterview'), ('Shortlisted', 'Shortlisted'), ('Negotiation', 'Negotiation'), ('Onboarding', 'Onboarding'), ('Closed', 'Closed')], default='Created', max_length=15),
        ),
    ]
