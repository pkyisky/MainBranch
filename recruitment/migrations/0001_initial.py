# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-31 15:42
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('organization', '0002_auto_20180501_1037'),
    ]

    operations = [
        migrations.CreateModel(
            name='Jobs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('jobtype', models.CharField(choices=[('Full Time', 'Full Time'), ('Contract', 'Contract'), ('Intern', 'Intern')], default='Intern', max_length=15)),
                ('skill', models.CharField(max_length=200)),
                ('contacts', models.ManyToManyField(related_name='jobHeading', to=settings.AUTH_USER_MODEL)),
                ('department', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='department_a', to='organization.Departments')),
                ('role', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='position_a', to='organization.Role')),
                ('unit', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='unit_a', to='organization.Unit')),
            ],
        ),
    ]
