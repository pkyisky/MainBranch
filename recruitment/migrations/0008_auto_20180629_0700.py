# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-29 07:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recruitment', '0007_jobs_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jobs',
            name='status',
            field=models.CharField(choices=[('Created', 'Created'), ('Active', 'Active'), ('Closed', 'Closed')], default='Created', max_length=15),
        ),
    ]
