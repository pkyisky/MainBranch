# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-01 10:37
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Roles',
            new_name='Role',
        ),
        migrations.RenameModel(
            old_name='Units',
            new_name='Unit',
        ),
        migrations.RenameField(
            model_name='departments',
            old_name='units',
            new_name='unit',
        ),
    ]
