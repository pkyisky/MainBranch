# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-28 08:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0003_auto_20180428_0846'),
    ]

    operations = [
        migrations.AddField(
            model_name='documentcontent',
            name='text',
            field=models.CharField(max_length=3000, null=True),
        ),
    ]
